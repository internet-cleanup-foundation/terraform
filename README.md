# Terraform

Configuration management for Internet Cleanup Foundation infrastructure.

## Usage

To setup run:

    make setup

Get a API token for Uptime Robot (https://uptimerobot.com/dashboard#mySettings -> API Settings -> Main API Key) and run:

    terraform apply -var 'uptimerobot_token=<ze-token>'
