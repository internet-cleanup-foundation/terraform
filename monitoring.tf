# set shell environment variable TF_VARS_uptimerobot_token with the API token:
# export TF_VARS_uptime_robot="<BLABLA_API_TOKEN>"
# https://uptimerobot.com/dashboard#mySettings -> API Settings -> Main API Key
variable "uptimerobot_token" {}

variable "domains" {
  default = ["basisbeveiliging.nl"]
}

provider "uptimerobot" {
  api_key = "${var.uptimerobot_token}"
}

resource "uptimerobot_alert_contact" "bofh" {
  friendly_name = "BOFH"
  type          = "email"
  value         = "bofh@internetcleanup.foundation"
}

resource "uptimerobot_monitor" "www-redirect" {
  count         = "${length(var.domains)}"
  friendly_name = "www-redirect ${element(var.domains, count.index)}"
  type          = "http"
  url           = "https://www.${element(var.domains, count.index)}"

  interval = "${24*60*60}"

  alert_contact {
    id = "${uptimerobot_alert_contact.bofh.id}"
  }
}

resource "uptimerobot_monitor" "ping" {
  count         = "${length(var.domains)}"
  friendly_name = "ping ${element(var.domains, count.index)}"
  type          = "ping"
  url           = "${element(var.domains, count.index)}"

  interval = "${60*60}"

  alert_contact {
    id = "${uptimerobot_alert_contact.bofh.id}"
  }
}

resource "uptimerobot_monitor" "http" {
  count         = "${length(var.domains)}"
  friendly_name = "http ${element(var.domains, count.index)}"
  type          = "http"
  url           = "http://${element(var.domains, count.index)}"

  interval = "${24*60*60}"

  alert_contact {
    id = "${uptimerobot_alert_contact.bofh.id}"
  }
}

resource "uptimerobot_monitor" "availability" {
  count         = "${length(var.domains)}"
  friendly_name = "availability ${element(var.domains, count.index)}"
  type          = "http"
  url           = "https://${element(var.domains, count.index)}"

  interval = "${15*60}"

  alert_contact {
    id = "${uptimerobot_alert_contact.bofh.id}"
  }
}

# currently doesn't work, maybe the check only read up until a certain limit in the content?
resource "uptimerobot_monitor" "content-rendering" {
  count         = "${length(var.domains)}"
  friendly_name = "content-rendering ${element(var.domains, count.index)}"
  type          = "keyword"
  url           = "https://${element(var.domains, count.index)}"

  # check for keyword in the footer, ensuring page is complete (enough) rendered
  keyword_type  = "exists"
  keyword_value = "MSPAINT.EXE"

  interval = "${60*60}"

  alert_contact {
    id = "${uptimerobot_alert_contact.bofh.id}"
  }
}

resource "uptimerobot_status_page" "main" {
  count         = "${length(var.domains)}"
  friendly_name = "${element(var.domains, count.index)}"
  custom_domain = "status.${element(var.domains, count.index)}"

  monitors = [
    "${uptimerobot_monitor.www-redirect.id}",
    "${uptimerobot_monitor.ping.id}",
    "${uptimerobot_monitor.http.id}",
    "${uptimerobot_monitor.availability.id}",
    "${uptimerobot_monitor.content-rendering.id}",
  ]
}
