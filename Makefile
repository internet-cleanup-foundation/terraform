all: setup

TMPDIR ?= /tmp

platform = linux_amd64

terraform_version = 0.11.11
terraform_url = https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_${platform}.zip

uptimerobot_plugin_version = 0.2.0
uptimerobot_plugin_url = https://github.com/louy/terraform-provider-uptimerobot/releases/download/v${uptimerobot_plugin_version}/terraform-provider-uptimerobot_${uptimerobot_plugin_version}_${platform}.tar.gz

plan: | setup
	terraform plan

apply: | setup
	terraform apply

setup: .bin/terraform .terraform/plugins/${platform}/terraform-provider-uptimerobot /usr/bin/git-crypt

.bin/terraform: | .bin
	cd ${TMPDIR}; wget ${terraform_url}
	cd .bin/; unzip -o ${TMPDIR}/terraform_${terraform_version}_${platform}.zip

.terraform/plugins/${platform}/terraform-provider-uptimerobot: | .terraform/plugins/${platform}
	cd ${@D}; curl -Ls ${uptimerobot_plugin_url} | tar zxf - ${@F}

.terraform/plugins/${platform} .bin:
	mkdir -p $@

clean:
	rm -rf .bin/* .terraform/plugins/*
